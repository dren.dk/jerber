package dk.dren.jerber.parser;

import dk.dren.jerber.parser.fileattribute.FileFunction;
import dk.dren.jerber.parser.geometry.BoundingBox;
import dk.dren.jerber.parser.lowlevel.GerberFileParser;
import org.apache.commons.io.FileUtils;
import org.decimal4j.immutable.Decimal6f;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.junit.Assert.*;

public class GerberFabricationSetTest {

    @Test
    public void loadZipsAndPanelize() throws IOException {

        Map<String, GerberFabricationSet> sets = new TreeMap<>();

        for (String zn : new String[]{"pod", "breakout"}) {
            String zipName = "mso5k-la-2019-02-03." + zn + ".zip";
            File zip = File.createTempFile("gerbs.", ".zip");
            try (InputStream is = GerberFileParser.class.getResourceAsStream("/" + zipName)) {
                FileUtils.copyInputStreamToFile(is, zip);
            }

            try {
                sets.put(zn, GerberFabricationSet.loadZip(zip));

            } finally {
                zip.delete();
            }
        }

        Set<FileFunction> layers = new TreeSet<>();
        for (GerberFabricationSet gerberFabricationSet : sets.values()) {
            layers.addAll(gerberFabricationSet.getFileByFunction().keySet());
        }

        GerberFabricationSet targetSet = new GerberFabricationSet("Panel");

        Decimal6f xOffset = Decimal6f.ZERO;
        for (Map.Entry<String, GerberFabricationSet> nameAndSet : sets.entrySet()) {
            GerberFabricationSet source = nameAndSet.getValue();
            BoundingBox boundingBox = source.getBoundingBox();
            if (boundingBox.isUnset()) {
                continue;
            }

            System.out.println("Adding "+nameAndSet.getKey());
            Decimal6f yOffset = Decimal6f.ZERO;
            targetSet.append(source,
                    xOffset.subtract(boundingBox.getMin().getX()),
                    yOffset.subtract(boundingBox.getMin().getY()));

            yOffset = yOffset.add(100);
            targetSet.append(source,
                    xOffset.subtract(boundingBox.getMin().getX()),
                    yOffset.subtract(boundingBox.getMin().getY()));

            xOffset = xOffset.add(100);
        }

        targetSet.store(new File("/tmp/jerber-panel.zip"));

        targetSet.renderSvg(new File("/tmp/jerber-panel.svg"));
    }


    @Test
    public void loadZipsAndPanelizeRenderOnlyProfile() throws IOException {

        Map<String, GerberFabricationSet> sets = new TreeMap<>();

        for (String zn : new String[]{"pod", "breakout"}) {
            String zipName = "mso5k-la-2019-02-03." + zn + ".zip";
            File zip = File.createTempFile("gerbs.", ".zip");
            try (InputStream is = GerberFileParser.class.getResourceAsStream("/" + zipName)) {
                FileUtils.copyInputStreamToFile(is, zip);
            }

            try {
                sets.put(zn, GerberFabricationSet.loadZip(zip));

            } finally {
                zip.delete();
            }
        }

        Set<FileFunction> layers = new TreeSet<>();
        for (GerberFabricationSet gerberFabricationSet : sets.values()) {
            layers.addAll(gerberFabricationSet.getFileByFunction().keySet());
        }

        GerberFabricationSet targetSet = new GerberFabricationSet("Panel");
        for (FileFunction layer : layers) {
            if (!layer.getType().equals("Profile")) {
                continue;
            }
            GerberFile target = new GerberFile("Only-panel");
            target.append(layer.toGerberCommand());

            Decimal6f xOffset = Decimal6f.ZERO;
            for (Map.Entry<String, GerberFabricationSet> nameAndSet : sets.entrySet()) {
                GerberFabricationSet gfs = nameAndSet.getValue();
                BoundingBox boundingBox = gfs.getBoundingBox();
                if (boundingBox.isUnset()) {
                    continue;
                }

                GerberFile gerberFile = gfs.getFileByFunction().get(layer);
                Assert.assertNotNull("Did not find the expected layer "+layer+" of "+ nameAndSet.getKey(), gerberFile);

                System.out.println("Adding "+layer+" from "+nameAndSet.getKey());
                Decimal6f yOffset = Decimal6f.ZERO;
                target.append(gerberFile, target.offset(xOffset.subtract(boundingBox.getMin().getX()), yOffset.subtract(boundingBox.getMin().getY())));
                yOffset = yOffset.add(100);
                target.append(gerberFile, target.offset(xOffset.subtract(boundingBox.getMin().getX()), yOffset.subtract(boundingBox.getMin().getY())));

                xOffset = xOffset.add(100);
            }

            targetSet.addFile(layer, target);
        }

        targetSet.store(new File("/tmp/jerber-panel-only-profile.zip"));

        targetSet.renderSvg(new File("/tmp/jerber-panel-only-profile.svg"));
    }


}