package dk.dren.jerber.parser.util;

import dk.dren.jerber.parser.cmd.UnitMO;
import org.decimal4j.immutable.Decimal6f;
import org.junit.Assert;
import org.junit.Test;

public class UnitConverterTest {

    @Test
    public void mm2mm() {
        UnitConverter uc = new UnitConverter(UnitMO.createMetric(), UnitMO.createMetric());
        Decimal6f from = Decimal6f.valueOf(47.23);
        Assert.assertEquals(Decimal6f.ONE, uc.getFromUnitsInMiliMeter());
        Assert.assertEquals(Decimal6f.ONE, uc.getToUnitsInMilimeter());
        Assert.assertEquals(from, uc.scaledToScaled(from));
        Assert.assertEquals(1000*1000L, uc.mmToTargetUnscaled(Decimal6f.ONE));
    }

    @Test
    public void insane2mm() {
        UnitConverter uc = new UnitConverter(UnitMO.createInsane(), UnitMO.createMetric());
        Assert.assertEquals(Decimal6f.valueOf(254), uc.scaledToScaled(Decimal6f.valueOf(10)));
    }

    @Test
    public void mm2insane() {
        UnitConverter uc = new UnitConverter(UnitMO.createMetric(), UnitMO.createInsane());
        Assert.assertEquals(Decimal6f.valueOf(10), uc.scaledToScaled(Decimal6f.valueOf(254)));
    }

    @Test
    public void insane2insane() {
        UnitConverter uc = new UnitConverter(UnitMO.createInsane(), UnitMO.createInsane());
        Decimal6f from = Decimal6f.valueOf(25);
        Assert.assertEquals(from, uc.scaledToScaled(from));
    }

    @Test
    public void insaneLong2mmLong() {
        UnitConverter mm2In = new UnitConverter(true, 0, false, 4);
        long tens = mm2In.mmToTargetUnscaled(Decimal6f.valueOf("25.4"));

        UnitConverter uc = new UnitConverter(false, 4, true, 6);
        Assert.assertEquals(Decimal6f.valueOf("0.0001"), uc.getLongsPerFromUnit());
        Assert.assertEquals(254000000L, uc.unscaledToUnscaled(100000));
        Assert.assertEquals(Decimal6f.valueOf("25.4"), uc.unscaledTomm(tens));
    }

    @Test
    public void um2nmLong() {
        UnitConverter uc = new UnitConverter(true, 3, true, 6);
        Assert.assertEquals(1234000L, uc.unscaledToUnscaled(1234));
    }

}