package dk.dren.jerber.parser.util;

import org.decimal4j.immutable.Decimal6f;
import org.junit.Assert;
import org.junit.Test;

public class NiceDecimalTest {

    @Test
    public void testIt() {
        Assert.assertEquals("1.23", NiceDecimal.format(new Decimal6f("1.23")));
        Assert.assertEquals("1", NiceDecimal.format(new Decimal6f("1.000")));
        Assert.assertEquals("1.0001", NiceDecimal.format(new Decimal6f("1.0001")));
        Assert.assertEquals("1100", NiceDecimal.format(new Decimal6f("1100")));
    }
}