package dk.dren.jerber.parser.cmd;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CoordinateDataTest {

    @Test
    public void testIt() {
        CoordinateData cd = new CoordinateData("X1Y-7J3I-9");
        Assert.assertEquals(new Long(1),cd.get(CoordinateData.Axis.X));
        Assert.assertEquals(new Long(-7),cd.get(CoordinateData.Axis.Y));
        Assert.assertEquals(new Long(3),cd.get(CoordinateData.Axis.J));
        Assert.assertEquals(new Long(-9),cd.get(CoordinateData.Axis.I));
    }

}