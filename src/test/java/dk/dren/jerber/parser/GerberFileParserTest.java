package dk.dren.jerber.parser;

import dk.dren.jerber.parser.cmd.GerberCommand;
import dk.dren.jerber.parser.lowlevel.GerberFileParser;
import dk.dren.jerber.parser.lowlevel.GerberTokenizer;
import org.apache.commons.io.FileUtils;
import org.decimal4j.immutable.Decimal6f;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class GerberFileParserTest {
    @Test
    public void testTwoSquare() throws IOException {
        List<GerberCommand> allCommands = readAllCommands("2-13-1_Two_square_boxes.gbr");
        Assert.assertEquals(19, allCommands.size());
    }

    @Test
    public void testPolarirites() throws IOException {
        List<GerberCommand> allCommands = readAllCommands("2-13-2_Polarities_and_Apertures.gbr");
        Assert.assertEquals(87, allCommands.size());
    }

    @Test
    public void readMSO5k() throws IOException {
        for (String zipName : new String[]{"mso5k-la-2019-02-03.pod.zip", "mso5k-la-2019-02-03.breakout.zip"}) {
            File zip = File.createTempFile("gerbs.",".zip");
            try (InputStream is = GerberFileParser.class.getResourceAsStream("/" + zipName)) {
                FileUtils.copyInputStreamToFile(is, zip);
            }

            try (ZipFile z = new ZipFile(zip)) {
                z.stream().filter(e->((ZipEntry) e).getName().endsWith(".gbr")).forEach(zipEntry->{

                    String entryName = zipName + "#" + zipEntry.getName();
                    try (InputStreamReader is = new InputStreamReader(z.getInputStream(zipEntry), StandardCharsets.UTF_8)) {
                        GerberFile gf = new GerberFile(entryName, is);

                        File f = new File("/tmp/jerber/"+((ZipEntry) zipEntry).getName());
                        f.getParentFile().mkdirs();
                        gf.store(f);

                    } catch (IOException e1) {
                        throw new RuntimeException("Failed while parsing "+entryName, e1);
                    }
                });

            } finally {
                zip.delete();
            }
        }
    }

    private List<GerberCommand> readAllCommands(String name) throws IOException {
        List<GerberCommand> res = new ArrayList<>();
        String fileName = "/Gerber_File_Format_Examples_20181113/" + name;
        try (GerberTokenizer gt = new GerberTokenizer(fileName,
                new InputStreamReader(GerberFileParserTest.class.getResourceAsStream(fileName)));
             GerberFileParser gf = new GerberFileParser(gt)) {
            while (gf.hasNext()) {
                res.add(gf.next());
            }
        }
        return res;
    }
}