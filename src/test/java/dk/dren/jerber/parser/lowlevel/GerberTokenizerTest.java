package dk.dren.jerber.parser.lowlevel;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStreamReader;

public class GerberTokenizerTest {

    @Test
    public void twoBoxes() throws IOException {
        Assert.assertEquals("G04 Ucamco ex. 1: Two square boxes*\n" +
                "FSLAX25Y25*\n" +
                "MOMM*\n" +
                "TF.Part,Other,example*\n" +
                "LPD*\n" +
                "ADD10C,0.010*\n" +
                "D10*\n" +
                "X0Y0D02*\n" +
                "G01*\n" +
                "X500000Y0D01*\n" +
                "Y500000D01*\n" +
                "X0D01*\n" +
                "Y0D01*\n" +
                "X600000D02*\n" +
                "X1100000D01*\n" +
                "Y500000D01*\n" +
                "X600000D01*\n" +
                "Y0D01*\n" +
                "M02*\n", read("2-13-1_Two_square_boxes.gbr"));
    }

    @Test
    public void nestedBlocks() throws IOException {
        Assert.assertEquals("G04 Ucamco copyright*\n" +
                "TF.GenerationSoftware,Ucamco,UcamX,2016.04-160425*\n" +
                "TF.CreationDate,2016-04-25T00:00:00+01:00*\n" +
                "FSLAX46Y46*\n" +
                "MOMM*\n" +
                "G04 Define standard apertures*\n" +
                "ADD10C,7.500000*\n" +
                "ADD11C,15*\n" +
                "ADD12R,20X10*\n" +
                "ADD13R,10X20*\n" +
                "G04 Define block aperture D100, consisting of two draws and a round dot*\n" +
                "ABD100*\n" +
                "D10*\n" +
                "X65532000Y17605375D02*\n" +
                "Y65865375D01*\n" +
                "X-3556000D01*\n" +
                "D11*\n" +
                "X-3556000Y17605375D03*\n" +
                "AB*\n" +
                "G04 Define block aperture  D102, consisting of 2x3 flashes of D101 and 1 flash of D12*\n" +
                "ABD102*\n" +
                "G04 Define nested block aperture D101, consisting of 2x2 flashes of D100*\n" +
                "ABD101*\n" +
                "D100*\n" +
                "X0Y0D03*\n" +
                "X0Y70000000D03*\n" +
                "X100000000Y0D03*\n" +
                "X100000000Y70000000D03*\n" +
                "AB*\n" +
                "D101*\n" +
                "X0Y0D03*\n" +
                "X0Y160000000D03*\n" +
                "X0Y320000000D03*\n" +
                "X230000000Y0D03*\n" +
                "X230000000Y160000000D03*\n" +
                "X230000000Y320000000D03*\n" +
                "D12*\n" +
                "X19500000Y-10000000D03*\n" +
                "AB*\n" +
                "G04 Flash D13 twice outside of blocks*\n" +
                "D13*\n" +
                "X-30000000Y10000000D03*\n" +
                "X143000000Y-30000000D03*\n" +
                "G04 Flash block D102 3x2 times*\n" +
                "D102*\n" +
                "X0Y0D03*\n" +
                "X0Y520000000D03*\n" +
                "X500000000Y0D03*\n" +
                "X500000000Y520000000D03*\n" +
                "X1000000000Y0D03*\n" +
                "X1000000000Y520000000D03*\n" +
                "M02*\n", read("4-6-4_Nested_blocks.gbr"));
    }

    private static String read(String name) throws IOException {
        StringBuilder sb = new StringBuilder();
        String fileName = "/Gerber_File_Format_Examples_20181113/" + name;
        try (GerberTokenizer gt = new GerberTokenizer(fileName, new InputStreamReader(GerberTokenizerTest.class
                .getResourceAsStream(fileName)))) {
            while (gt.hasNext()) {
                sb.append(gt.next()).append("\n");
            }
        }
        return sb.toString();
    }
}