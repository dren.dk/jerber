package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.lowlevel.FileLocation;
import lombok.RequiredArgsConstructor;

import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
public class RegExpParser implements DrillCommandParser {
    private final Pattern pattern;
    private final BiFunction<FileLocation, Matcher, DrillCommand> constructor;

    @Override
    public DrillCommand tryParse(FileLocation location, String line) {
        Matcher matcher = pattern.matcher(line);
        if (matcher.matches()) {
            return constructor.apply(location, matcher);
        }
        return null;
    }
}
