package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.lowlevel.FileLocation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@Getter
public class Comment implements DrillCommand {
    public static final DrillCommandParser PARSER = new RegExpParser(Pattern.compile(";(.+)"), Comment::new);
    private final String text;

    public Comment(FileLocation fileLocation, Matcher matcher) {
        text = matcher.group(1);
    }

    @Override
    public String toDrillCommand() {
        return ";"+text;
    }
}
