package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.lowlevel.FileLocation;
import lombok.NoArgsConstructor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@NoArgsConstructor
public class StartHeaderM48 implements DrillCommand {

    public static final DrillCommandParser PARSER = new RegExpParser(Pattern.compile("M48"), StartHeaderM48::new);

    public StartHeaderM48(FileLocation fileLocation, Matcher matcher) {

    }

    @Override
    public String toDrillCommand() {
        return "M48";
    }
}
