package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.DrillFile;
import dk.dren.jerber.parser.geometry.Point6f;
import dk.dren.jerber.parser.lowlevel.FileLocation;
import dk.dren.jerber.parser.render.Aperture;
import dk.dren.jerber.parser.render.RenderTarget;
import dk.dren.jerber.parser.util.NiceDecimal;
import lombok.RequiredArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
public class SelectToolT implements DrillCommand {
    public static final DrillCommandParser PARSER = new RegExpParser(Pattern.compile("T(\\d+)"), SelectToolT::new);
    private final Integer number;

    public SelectToolT(FileLocation fileLocation, Matcher matcher) {
        number = Integer.valueOf(matcher.group(1));
    }

    @Override
    public String toDrillCommand() {
        return "T"+number;
    }

    @Override
    public void append(DrillFile source, DrillFile target, Decimal6f xOffset, Decimal6f yOffset) {
        if (number == 0) {
            return;
        }

        Decimal6f size = source.getDrillIndex().getDrillSizeForNumber(number);
        if (size == null) {
            throw new RuntimeException("Could not find tool number T"+number+" in "+source.getFileName());
        }
        Integer targetNumber = target.getDrillIndex().addDrill(new ToolDefinition(number, size));
        target.append(new SelectToolT(targetNumber));
    }

    @Override
    public void render(DrillFile drillFile, RenderTarget renderTarget) {
        Decimal6f size = drillFile.getDrillIndex().getDrillSizeForNumber(number).divide(2);
        renderTarget.setAperture(new Aperture() {
            @Override
            public String getSvgPathStyle() {
                return "";
            }

            @Override
            public String flashSvg(Point6f point) {
                return "<circle " +
                        "style=\"fill:red;stroke:none;\" " +
                        "cx=\""+NiceDecimal.format(point.getX())+"\" " +
                        "cy=\""+NiceDecimal.format(point.getY().negate())+"\" " +
                        "r=\""+NiceDecimal.format(size)+"\" />";
            }
        });
    }
}
