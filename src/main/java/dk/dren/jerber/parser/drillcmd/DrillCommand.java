package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.DrillFile;
import dk.dren.jerber.parser.render.RenderTarget;
import org.decimal4j.immutable.Decimal6f;

public interface DrillCommand {
    String toDrillCommand();
    default void append(DrillFile source, DrillFile target, Decimal6f xOffset, Decimal6f yOffset) {};

    default void render(DrillFile drillFile, RenderTarget renderTarget) {}
}
