package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.lowlevel.FileLocation;
import lombok.NoArgsConstructor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@NoArgsConstructor
public class Metric implements DrillCommand {
    public static final DrillCommandParser PARSER = new RegExpParser(Pattern.compile("METRIC,TZ"), Metric::new);

    public Metric(FileLocation fileLocation, Matcher matcher) {

    }

    @Override
    public String toDrillCommand() {
        return "METRIC,TZ";
    }
}
