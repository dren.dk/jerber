package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.lowlevel.FileLocation;

public interface DrillCommandParser {
    DrillCommand tryParse(FileLocation location, String line);
}
