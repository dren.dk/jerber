package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.DrillFile;
import dk.dren.jerber.parser.lowlevel.FileLocation;
import dk.dren.jerber.parser.util.NiceDecimal;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@RequiredArgsConstructor
public class ToolDefinition implements DrillCommand {

    public static final DrillCommandParser PARSER = new RegExpParser(Pattern.compile("T(\\d+)C([\\d.]+)"), ToolDefinition::new);
    private final Integer number;
    private final Decimal6f size;

    public ToolDefinition(FileLocation fileLocation, Matcher matcher) {
        number = Integer.valueOf(matcher.group(1));
        size = Decimal6f.valueOf(matcher.group(2));
    }

    @Override
    public String toDrillCommand() {
        return "T"+number+"C"+ NiceDecimal.formatWithDot(size);
    }

    @Override
    public void append(DrillFile source, DrillFile target, Decimal6f xOffset, Decimal6f yOffset) {
        target.append(this);
    }
}
