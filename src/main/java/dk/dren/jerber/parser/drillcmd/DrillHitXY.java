package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.DrillFile;
import dk.dren.jerber.parser.geometry.Point6f;
import dk.dren.jerber.parser.lowlevel.FileLocation;
import dk.dren.jerber.parser.render.RenderTarget;
import dk.dren.jerber.parser.util.NiceDecimal;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@RequiredArgsConstructor
public class DrillHitXY implements DrillCommand {

    public static final DrillCommandParser PARSER = new RegExpParser(Pattern.compile("X([+-]?[\\d+.]+)Y([+-]?[\\d+.]+)"), DrillHitXY::new);
    private final Point6f point;

    public DrillHitXY(FileLocation fileLocation, Matcher matcher) {
        point = new Point6f(Decimal6f.valueOf(matcher.group(1)), Decimal6f.valueOf(matcher.group(2)));
    }

    @Override
    public String toDrillCommand() {
        return "X"+NiceDecimal.formatWithDot(point.getX())+"Y"+NiceDecimal.formatWithDot(point.getY());
    }

    @Override
    public void append(DrillFile source, DrillFile target, Decimal6f xOffset, Decimal6f yOffset) {
        target.append(new DrillHitXY(point.add(xOffset, yOffset)));
    }

    @Override
    public void render(DrillFile drillFile, RenderTarget renderTarget) {
        renderTarget.flash(point);
    }
}
