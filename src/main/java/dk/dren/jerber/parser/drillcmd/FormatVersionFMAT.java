package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.lowlevel.FileLocation;
import lombok.NoArgsConstructor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@NoArgsConstructor
public class FormatVersionFMAT implements DrillCommand {
    public static final DrillCommandParser PARSER = new RegExpParser(Pattern.compile("FMAT,2"), FormatVersionFMAT::new);

    public FormatVersionFMAT(FileLocation fileLocation, Matcher matcher) {

    }

    @Override
    public String toDrillCommand() {
        return "FMAT,2";
    }
}
