package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.lowlevel.FileLocation;
import lombok.RequiredArgsConstructor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
public class DrillModeG05 implements DrillCommand {
    public static final DrillCommandParser PARSER = new RegExpParser(Pattern.compile("G05"), DrillModeG05::new);

    public DrillModeG05(FileLocation fileLocation, Matcher matcher) {

    }

    @Override
    public String toDrillCommand() {
        return "G05";
    }
}
