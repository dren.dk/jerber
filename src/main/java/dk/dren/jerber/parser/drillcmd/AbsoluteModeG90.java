package dk.dren.jerber.parser.drillcmd;

import dk.dren.jerber.parser.lowlevel.FileLocation;
import lombok.NoArgsConstructor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@NoArgsConstructor
public class AbsoluteModeG90 implements DrillCommand {

    public static final DrillCommandParser PARSER = new RegExpParser(Pattern.compile("G90"), AbsoluteModeG90::new);

    public AbsoluteModeG90(FileLocation fileLocation, Matcher matcher) {

    }

    @Override
    public String toDrillCommand() {
        return "G90";
    }
}
