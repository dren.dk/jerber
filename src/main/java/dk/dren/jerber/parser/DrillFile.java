package dk.dren.jerber.parser;

import dk.dren.jerber.parser.drillcmd.*;
import dk.dren.jerber.parser.lowlevel.DrillFileParser;
import dk.dren.jerber.parser.lowlevel.DrillIndex;
import dk.dren.jerber.parser.render.RenderTarget;
import dk.dren.jerber.parser.render.SvgLayerRenderTarget;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * An Excellon drill file, with the drill index as a separate data structure to allow merging
 *
 * At the moment only pure drill files are supported iow. not routing.
 */
@RequiredArgsConstructor
@Getter
public class DrillFile {
    private final String fileName;
    private List<DrillCommand> commands = new ArrayList<>();
    private DrillIndex drillIndex = new DrillIndex();

    private static final List<DrillCommand> START_HEADER = Arrays.asList(
            new StartHeaderM48(),
            new Comment("Drill file"),
            new Comment("FORMAT={-:-/ absolute / metric / decimal}"),
            new FormatVersionFMAT(),
            new Metric()
    );
    private static final List<DrillCommand> END_HEADER = Arrays.asList(
            new RewindStop(),
            new AbsoluteModeG90(),
            new DrillModeG05()
    );

    private static final List<DrillCommand> END_FILE = Arrays.asList(
            new SelectToolT(0),
            new EndOfProgramM30()
    );

    public enum DrillType {
        PLATED, NON_PLATED
    }

    public DrillFile(String fileName, InputStreamReader is) {
        DrillFileParser drillFileParser = new DrillFileParser(fileName, is, this);
        drillFileParser.parse();
        this.fileName = fileName;
    }

    public void append(DrillCommand drillCommand) {
        if (drillCommand instanceof ToolDefinition) {
            drillIndex.addDrill((ToolDefinition) drillCommand);
        } else {
            commands.add(drillCommand);
        }
    }

    public void render(RenderTarget renderTarget) {
        for (DrillCommand command : commands) {
            command.render(this, renderTarget);
        }
    }

    public void append(DrillFile source, Decimal6f xOffset, Decimal6f yOffset) {
        for (DrillCommand sourceCommand : source.getCommands()) {
            sourceCommand.append(source, this, xOffset, yOffset);
        }
    }

    public void store(Writer writer) throws IOException {
        store(writer, START_HEADER);
        store(writer, drillIndex.commands());
        store(writer, END_HEADER);
        store(writer, commands);
        store(writer, END_FILE);
    }

    private void store(Writer writer, List<DrillCommand> commands) throws IOException {
        for (DrillCommand command : commands) {
            storeCommand(writer, command);
        }
    }

    private void storeCommand(Writer writer, DrillCommand command) throws IOException {
        writer.append(command.toDrillCommand());
        writer.append("\n");
    }

}
