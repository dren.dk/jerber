package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.DataBlock;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.lowlevel.ParseException;
import dk.dren.jerber.parser.lowlevel.FileLocation;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 4.5 Aperture Macro (AM)
 */
@Getter
public class ApertureMacroAM implements GerberCommand {

    public static final GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("AM(.+)"), ApertureMacroAM::new);
    private final String name;
    private final FileLocation location;
    private final List<DataBlock> datablocks;

    @Setter
    private GerberFile gerberFile;

    public ApertureMacroAM(Matcher matcher, ExtendedCommand extendedCommand) {
        datablocks = extendedCommand.getDatablocks();
        location = datablocks.get(0).getLocation();
        name = matcher.group(1);
    }

    @Override
    public String getGerberString() {
        StringBuilder sb = new StringBuilder();
        for (DataBlock datablock : datablocks) {
            sb.append(datablock.toString());
        }

        return "%"+sb.toString()+"%";
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        throw new ParseException(getLocation(), "Appending Aperture macros are not implemented because they aren't used by any tool I use" +
                "and they are HORRENDOUSLY complicated to support if a source file is defined in INsane and you want to append it to" +
                "a metric file, if you feel like wading through the legacy swamp that is AM command, feel free to send a me a pull-request.\n"+getGerberString());
    }
}
