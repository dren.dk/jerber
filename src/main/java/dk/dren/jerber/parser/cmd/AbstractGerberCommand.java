package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.DataBlock;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.lowlevel.ParseException;
import dk.dren.jerber.parser.lowlevel.FileLocation;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * This is just a handy place to stick the location that all gerber commands will have when they have come from a file
 * Another nice thing is that it contains a check that enforces that there's exactly one datablock in the loaded command
 */
@Getter
public abstract class AbstractGerberCommand implements GerberCommand {
    private final FileLocation location;

    @Setter
    private GerberFile gerberFile;

    public AbstractGerberCommand(ExtendedCommand extendedCommand) {
        if (extendedCommand != null) {
            List<DataBlock> datablocks = extendedCommand.getDatablocks();
            if (datablocks.isEmpty()) {
                location = null;
            } else {
                location = datablocks.get(0).getLocation();
            }
            if (datablocks.size() != 1) {
                throw new ParseException(location, "Bad number of datablocks found there should be exactly 1, not "+datablocks.size());
            }
        } else {
            location = null;
        }
    }
}
