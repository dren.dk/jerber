package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
public class DeleteAttributeTD extends StaticGerberCommand {
    public static final GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("TD(.*)"), DeleteAttributeTD::new);
    private final String attributeName;

    public DeleteAttributeTD(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand, true);
        this.attributeName = matcher.group(1);
    }
}
