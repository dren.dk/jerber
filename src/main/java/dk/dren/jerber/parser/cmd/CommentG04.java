package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 4.14 Comment (G04)
 * https://www.ucamco.com/files/downloads/file/81/the_gerber_file_format_specification.pdf?eac5a8c1daa8a260a3076bace3087513
 */
@Getter
public class CommentG04 extends AbstractGerberCommand {
    public static GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("G04 (.+)"), CommentG04::new);

    public static final String RESERVED_PREFIX = "#@!";
    private final String comment;

    public CommentG04(Matcher m, ExtendedCommand extendedCommand) {
        super(extendedCommand);
        this.comment = m.group(1);
    }

    public CommentG04(String comment) {
        super(null);
        this.comment = comment;
    }

    @Override
    public String getGerberString() {
        return "G04 "+comment+"*";
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        targetGerberFile.append(this);
    }
}
