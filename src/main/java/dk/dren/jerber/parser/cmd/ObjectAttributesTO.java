package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
public class ObjectAttributesTO extends StaticGerberCommand {
    public static final GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("TO([^,]+)(,(.+))"), ObjectAttributesTO::new);
    private final String name;
    private final List<String> values;

    public ObjectAttributesTO(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand, true);
        name = matcher.group(1);
        values = matcher.group(2) != null ? Arrays.asList(matcher.group(3).split(",")) : Collections.emptyList();
    }
}
