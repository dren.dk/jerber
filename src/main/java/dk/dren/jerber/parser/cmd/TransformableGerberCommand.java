package dk.dren.jerber.parser.cmd;

public interface TransformableGerberCommand<T extends GerberCommand> {
    T transform(GerberTransform transform);

}
