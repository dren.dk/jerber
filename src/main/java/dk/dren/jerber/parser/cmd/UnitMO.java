package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import dk.dren.jerber.parser.render.RenderTarget;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 4.2 Unit (MO)
 *
 * MOMM sets the unit to be mm
 * MOIN sets the unit to be insane
 */
@Getter
public class UnitMO extends AbstractGerberCommand {
    public static final GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("MO(MM|IN)"), UnitMO::new);
    private final boolean metric;

    public UnitMO(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand);
        metric = matcher.group(1).equals("MM");
    }

    private UnitMO(boolean metric) {
        super(null);
        this.metric = metric;
    }

    public static UnitMO createMetric() {
        return new UnitMO(true);
    }

    public static UnitMO createInsane() {
        return new UnitMO(false);
    }

    @Override
    public String getGerberString() {
        return metric ? "%MOMM*%" : "%MOIN*%";
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        if (targetGerberFile.getUnit() == null) {
            targetGerberFile.append(new UnitMO(metric));
        }
    }

    @Override
    public String toString() {
        return metric ? "metric" : "insane";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnitMO unitMO = (UnitMO) o;
        return metric == unitMO.metric;
    }

    @Override
    public void render(GerberRenderTarget renderTarget) {
        renderTarget.setMetric(metric);
    }
}
