package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.FileLocation;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import dk.dren.jerber.parser.render.RenderTarget;

public interface GerberCommand {
    /**
     * @return The source of this command, if available, otherwise it's null
     */
    FileLocation getLocation();

    /**
     * @return The gerber command ready for a file
     */
    String getGerberString();

    /**
     * Append the command to a gerber file.
     *
     * @param targetGerberFile The gerber file to append this command to
     * @param transform The transform to apply when appending, or null if none
     */
    void append(GerberFile targetGerberFile, GerberTransform transform);

    /**
     * @param gerberFile The GerberFile this command belongs to.
     */
    void setGerberFile(GerberFile gerberFile);
    GerberFile getGerberFile();

    default void render(GerberRenderTarget renderTarget) {}
}
