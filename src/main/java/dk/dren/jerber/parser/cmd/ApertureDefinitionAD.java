package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import dk.dren.jerber.parser.util.NiceDecimal;
import dk.dren.jerber.parser.util.UnitConverter;
import lombok.Getter;
import org.decimal4j.immutable.Decimal6f;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 4.3 Aperture Definition (ADD)
 */
@Getter
public class ApertureDefinitionAD extends AbstractGerberCommand {
    public static final GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("ADD(\\d+)([^,]+)(,(.+))?"), ApertureDefinitionAD::new);
    private final int dCode;
    private final String template;
    private final List<Decimal6f> modifiers;

    public ApertureDefinitionAD(int dCode, String template, List<Decimal6f> modifiers) {
        super(null);
        this.dCode = dCode;
        this.template = template;
        this.modifiers = modifiers;
    }

    public ApertureDefinitionAD(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand);
        dCode = Integer.parseInt(matcher.group(1));
        template = matcher.group(2);
        if (matcher.group(4) != null) {
            modifiers = Arrays.stream(matcher.group(4).split("X"))
                    .map(Decimal6f::valueOf)
                    .collect(Collectors.toList());
        } else {
            modifiers = Collections.emptyList();
        }
    }

    @Override
    public String getGerberString() {
        return String.format("%%ADD%02d", dCode) + getDefinition() + "*%";
    }

    public String getDefinition() {
        return getDefinition(this.modifiers);
    }

    public String getDefinition(List<Decimal6f> mods) {
        if (mods.isEmpty()) {
            return template;
        } else {
            return template + "," +  mods.stream()
                    .map(NiceDecimal::format)
                    .collect(Collectors.joining("X"));
        }
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        UnitConverter converter = getGerberFile().getXConverter(targetGerberFile);
        List<Decimal6f> newMods = getModifiers().stream()
                .map(converter::scaledToScaled)
                .collect(Collectors.toList());

        String newDefinition = getDefinition(newMods);
        ApertureDefinitionAD newAD = new ApertureDefinitionAD(dCode, getTemplate(), newMods);
        newAD.setGerberFile(targetGerberFile);
        targetGerberFile.append(newAD);
    }

    @Override
    public void render(GerberRenderTarget renderTarget) {
        renderTarget.defineAperture(this);
    }
}
