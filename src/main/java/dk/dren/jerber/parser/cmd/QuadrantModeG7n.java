package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.lowlevel.ParseException;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
public class QuadrantModeG7n extends StaticGerberCommand {
    public static final GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("G0*(7[45])"), QuadrantModeG7n::new);

    public enum QuadrantMode {
        SINGLE(74),
        MULTI(75);

        private final int code;

        QuadrantMode(int code) {
            this.code = code;
        }
    }

    private final QuadrantMode mode;

    public QuadrantModeG7n(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand, false);

        int code = Integer.valueOf(matcher.group(1));
        if (code == QuadrantMode.SINGLE.code) {
            mode = QuadrantMode.SINGLE;
        } else if (code == QuadrantMode.MULTI.code) {
            mode = QuadrantMode.MULTI;
        } else {
            throw new ParseException(getLocation(), "Unknown code: "+extendedCommand.getDatablocks().get(0));
        }
    }

    @Override
    public void render(GerberRenderTarget renderTarget) {
        renderTarget.setQuadrantMode(mode.equals(QuadrantMode.MULTI));
    }
}
