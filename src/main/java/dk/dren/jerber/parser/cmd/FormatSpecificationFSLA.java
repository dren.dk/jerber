package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.lowlevel.ParseException;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import dk.dren.jerber.parser.render.RenderTarget;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 4.1 Format Specification (FS)
 *
 * This little piece if crazyness defines how to read coordinate numbers in the file, because putting a decimal point
 * in the actual number or simply using nm would have been too much trouble.
 *
 * Note that this command allows x and y coordinates to be scaled differently, why? Because fuck implementors, that's why.
 *
 * There's no reason to think that anybody has ever used different scaling digits in x and y, so I'll treat that as an error
 * to avoid having to deal with such nonsense
 *
 * What dark madness possessed the designer of this command?
 */
@Getter
public class FormatSpecificationFSLA extends AbstractGerberCommand {
    public static GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("FSLAX(\\d)(\\d)Y(\\d)(\\d)"), FormatSpecificationFSLA::new);
    private final int xIntegerDigits;
    private final int xDecimalDigits;
    private final int yIntegerDigits;
    private final int yDecimalDigits;

    public FormatSpecificationFSLA(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand);
        xIntegerDigits = Integer.parseInt(matcher.group(1));
        xDecimalDigits = Integer.parseInt(matcher.group(2));
        yIntegerDigits = Integer.parseInt(matcher.group(3));
        yDecimalDigits = Integer.parseInt(matcher.group(4));

        sanityCheck();
    }

    private void sanityCheck() {
        if (xDecimalDigits != yDecimalDigits) {
            throw new ParseException(getLocation(), "insane format specification x decimals ("+xDecimalDigits+") different from y decimals ("+yDecimalDigits+")");
        }
    }

    /**
     * Creates a createMetric default format spec, which allows nm resolution for mm
     */
    private FormatSpecificationFSLA(int xIntegerDigits, int xDecimalDigits, int yIntegerDigits, int yDecimalDigits) {
        super(null);
        this.xIntegerDigits = xIntegerDigits;
        this.xDecimalDigits = xDecimalDigits;
        this.yIntegerDigits = yIntegerDigits;
        this.yDecimalDigits = yDecimalDigits;
        sanityCheck();
    }

    public static GerberCommand createNanoMeter() {
        return new FormatSpecificationFSLA(4,6,4,6);
    }

    @Override
    public String getGerberString() {
        return String.format("%%FSLAX%d%dY%d%d*%%", xIntegerDigits, xDecimalDigits, yIntegerDigits, yDecimalDigits);
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        if (targetGerberFile.getFormat() == null) {
            targetGerberFile.append(new FormatSpecificationFSLA(xIntegerDigits, xDecimalDigits, yIntegerDigits, yDecimalDigits));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FormatSpecificationFSLA that = (FormatSpecificationFSLA) o;
        return xIntegerDigits == that.xIntegerDigits &&
                xDecimalDigits == that.xDecimalDigits &&
                yIntegerDigits == that.yIntegerDigits &&
                yDecimalDigits == that.yDecimalDigits;
    }

    @Override
    public void render(GerberRenderTarget renderTarget) {
        renderTarget.setFixedDecimals(xDecimalDigits);
    }
}
