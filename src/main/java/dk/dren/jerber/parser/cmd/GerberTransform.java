package dk.dren.jerber.parser.cmd;

import lombok.Value;

/**
 * Offsets in unscaledToUnscaled, native units, iow: A GerberTransform is local to a single gerber file because
 * the units in the gerber file might not be the same as another one.
 */
@Value
public class GerberTransform {
    private Long xOffset;
    private Long yOffset;

    @Override
    public String toString() {
        return "GerberTransform{" +
                "xOffset=" + xOffset +
                ", yOffset=" + yOffset +
                '}';
    }
}
