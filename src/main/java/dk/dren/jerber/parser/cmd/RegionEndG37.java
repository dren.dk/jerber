package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import lombok.Getter;

@Getter
public class RegionEndG37 extends StaticGerberCommand {
    public static final GerberCommandParser PARSER = new StringCommandParser("G37", RegionEndG37::new);

    @Override
    public void render(GerberRenderTarget renderTarget) {
        renderTarget.regionEnd();
    }

    public RegionEndG37(ExtendedCommand extendedCommand) {
        super(extendedCommand, false);
    }
}
