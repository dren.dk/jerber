package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import lombok.RequiredArgsConstructor;

import java.util.function.Function;

/**
 * Simply matches a fixed string as a command
 */
@RequiredArgsConstructor
public class StringCommandParser implements GerberCommandParser {
    private final String commandString;
    private final Function<ExtendedCommand, GerberCommand> constructor;

    @Override
    public GerberCommand tryParse(ExtendedCommand extendedCommand) {
        if (extendedCommand.getDatablocks().get(0).getContent().equals(commandString)) {
            return constructor.apply(extendedCommand);
        } else {
            return null;
        }
    }
}
