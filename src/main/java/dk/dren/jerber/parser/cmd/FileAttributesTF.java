package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 5.2 File Attributes (TF)
 */
@Getter
public class FileAttributesTF extends AbstractGerberCommand {
    public static final GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("TF([^,]+),?(.*)"), FileAttributesTF::new);
    private final String name;
    private final List<String> values;

    public FileAttributesTF(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand);
        name = matcher.group(1);
        final String commaSeparatedListOfValues = matcher.group(2);
        values = Arrays.asList(commaSeparatedListOfValues.split(","));
    }

    public FileAttributesTF(String name, List<String>values) {
        super(null);
        this.name = name;
        this.values = values;
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        targetGerberFile.append(new CommentG04("Source "+name+"="+String.join(",", values)));
    }

    @Override
    public String getGerberString() {
        return "%TF"+name+","+String.join(",", values)+"%";
    }
}
