package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.lowlevel.ParseException;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import dk.dren.jerber.parser.render.RenderTarget;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 4.9 Linear Interpolation Mode (G01)
 */
@Getter
public class InterpolationModeG0n extends StaticGerberCommand {
    public static final GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("G0*([123])"), InterpolationModeG0n::new);
    private final int gCode;

    public InterpolationModeG0n(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand, false);
        gCode = Integer.parseInt(matcher.group(1));
        if (gCode < 1 || gCode > 3) {
            throw new ParseException(getLocation(), "Invalid Interpolation command expected either G01, G02 or G03");
        }
    }

    @Override
    public void render(GerberRenderTarget renderTarget) {
        renderTarget.setInterpolationMode(gCode);
    }
}
