package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 4.11.2 Load Polarity (LP)
 *
 * LPD sets dark polarity
 * LPC sets clear polarity
 */
@Getter
public class LoadPolarityLP extends StaticGerberCommand {
    public static final GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("LP(C|D)"), LoadPolarityLP::new);
    private final boolean dark;

    public LoadPolarityLP(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand, true);
        dark = matcher.group(1).equals("D");
    }

    @Override
    public void render(GerberRenderTarget renderTarget) {
        renderTarget.setPolarity(dark);
    }
}
