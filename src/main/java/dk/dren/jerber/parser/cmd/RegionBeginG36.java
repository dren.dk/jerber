package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
public class RegionBeginG36 extends StaticGerberCommand {
    public static final GerberCommandParser PARSER = new StringCommandParser("G36", RegionBeginG36::new);

    public RegionBeginG36(ExtendedCommand extendedCommand) {
        super(extendedCommand, false);
    }

    @Override
    public void render(GerberRenderTarget renderTarget) {
        renderTarget.regionBegin();
    }
}
