package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import lombok.Getter;


/**
 * A command that is not at all dependent on the context of the file it came from, so no coordinates or measurements
 */
@Getter
public class StaticGerberCommand extends AbstractGerberCommand {
    private final String gerberString;

    public StaticGerberCommand(ExtendedCommand extendedCommand, boolean percented) {
        super(extendedCommand);
        String quotes = percented ? "%" : "";
        gerberString = quotes+extendedCommand.getDatablocks().get(0).toString()+quotes;
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        targetGerberFile.append(this);
    }
}
