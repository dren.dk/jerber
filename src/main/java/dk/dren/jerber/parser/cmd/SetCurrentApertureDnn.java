package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.lowlevel.ParseException;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 4.7 Set Current Aperture (Dnn)
 */
@Getter
public class SetCurrentApertureDnn extends AbstractGerberCommand {
    public static final GerberCommandParser PARSER = new RegexpCommandParser(Pattern.compile("D(\\d+)"), SetCurrentApertureDnn::new);
    private final int dCode;

    public SetCurrentApertureDnn(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand);
        dCode = Integer.parseInt(matcher.group(1));
    }

    public SetCurrentApertureDnn(int dCode) {
        super(null);
        this.dCode = dCode;
    }

    @Override
    public String getGerberString() {
        return String.format("D%02d*", dCode);
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        String apertureDefinition = getGerberFile().getApertureDefinitionForDCode(dCode);
        if (apertureDefinition == null) {
            throw new ParseException(getLocation(), "Could not find a definition of "+getGerberString());
        }
        Integer targetDCode = targetGerberFile.getDCodeForApertureDefinition(apertureDefinition);
        targetGerberFile.append(new SetCurrentApertureDnn(targetDCode));
    }

    @Override
    public void render(GerberRenderTarget renderTarget) {
        renderTarget.setCurrentAperture(dCode);
    }
}
