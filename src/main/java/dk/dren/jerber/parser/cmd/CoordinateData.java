package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.util.UnitConverter;
import org.decimal4j.immutable.Decimal6f;

import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Pattern data as seen in the operations
 */
public class CoordinateData {
    public static Pattern PATTERN = Pattern.compile("((?:[XYIJ][+-]?[\\d]+)+)");
    public static Pattern SUB_PATTERN = Pattern.compile("([XYIJ])([+-]?[\\d]+)");
    public enum Axis {
        X,Y,I,J
    }

    private final Map<Axis, Long> coord;

    public CoordinateData(Map<Axis, Long> coord) {
        this.coord = coord;
    }

    public String getGerberString() {
        StringBuilder sb = new StringBuilder();
        for (Axis axis : Axis.values()) {
            Long theLong = coord.get(axis);
            if (theLong != null) {
                sb.append(axis.name()).append(theLong);
            }
        }
        return sb.toString();
    }

    public CoordinateData transform(GerberFile fromGerberFile, GerberFile toGerberFile, GerberTransform transform) {
        if (fromGerberFile == null) {
            throw new IllegalArgumentException("Need a from file");
        }
        if (toGerberFile == null) {
            throw new IllegalArgumentException("Need a to file");
        }
        UnitConverter xuc = fromGerberFile.getXConverter(toGerberFile);
        UnitConverter yuc = fromGerberFile.getYConverter(toGerberFile);

        Map<Axis, Long> newCoord = new TreeMap<>();
        Long xLong = get(Axis.X);
        if (xLong != null) {
            newCoord.put(Axis.X, xuc.unscaledToUnscaled(xLong)+transform.getXOffset());
        }
        Long yLong = get(Axis.Y);
        if (yLong != null) {
            newCoord.put(Axis.Y, yuc.unscaledToUnscaled(yLong)+transform.getYOffset());
        }
        Long iLong = get(Axis.I);
        if (iLong != null) {
            newCoord.put(Axis.I, xuc.unscaledToUnscaled(iLong));
        }
        Long jLong = get(Axis.J);
        if (jLong != null) {
            newCoord.put(Axis.J, yuc.unscaledToUnscaled(jLong));
        }

        return new CoordinateData(newCoord);
    }

    public Long get(Axis axis) {
        return coord.get(axis);
    }

    public CoordinateData(String inputString) {
        if (!PATTERN.matcher(inputString).matches()) {
            throw new IllegalArgumentException("Could not parse '"+inputString+"' as coordinate data");
        }

        coord = new TreeMap<>();
        Matcher parts = SUB_PATTERN.matcher(inputString);
        while (parts.find()) {
            coord.put(Axis.valueOf(parts.group(1)), Long.parseLong(parts.group(2)));
        }
    }
}
