package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.lowlevel.DataBlock;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import lombok.RequiredArgsConstructor;

import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple regexp based parser that parses a command by matching the string as a regular expression
 */
@RequiredArgsConstructor
public class RegexpCommandParser implements GerberCommandParser {
    private final Pattern pattern;
    private final BiFunction<Matcher, ExtendedCommand, GerberCommand> constructor;

    @Override
    public GerberCommand tryParse(ExtendedCommand extendedCommand) {
        DataBlock first = extendedCommand.getDatablocks().get(0);
        Matcher matcher = pattern.matcher(first.getContent());
        if (matcher.matches()) {
            return constructor.apply(matcher, extendedCommand);
        }
        return null;
    }
}
