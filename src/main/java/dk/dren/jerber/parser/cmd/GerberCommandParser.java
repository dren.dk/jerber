package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.lowlevel.ExtendedCommand;

public interface GerberCommandParser {
    GerberCommand tryParse(ExtendedCommand extendedCommand);
}
