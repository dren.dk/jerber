package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import lombok.Getter;

@Getter
public class EndOfFileM02 extends AbstractGerberCommand {
    public static final GerberCommandParser PARSER = new StringCommandParser("M02", EndOfFileM02::new);

    public EndOfFileM02() {
        super(null);
    }

    public EndOfFileM02(ExtendedCommand extendedCommand) {
        super(extendedCommand);
    }

    @Override
    public String getGerberString() {
        return "M02*";
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        // Never append the EOF command.
    }
}
