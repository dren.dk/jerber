package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.DataBlock;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.lowlevel.ParseException;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import dk.dren.jerber.parser.render.RenderTarget;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 4.8 Operations (D01/D02/D03)
 *
 * The operations have the following effect.
 *
 * Operation with D01 kind is called interpolate operation. It creates a straight-line segment or
 * a circular segment by interpolating from the current point to the operation coordinates. The
 * segment is then converted to a graphics object outside a region statement or to a contour
 * segment inside.
 *
 * Operation with D02 kind is called move operation. It moves the current point to the
 * operation coordinates. No graphics object is generated.
 *
 * Operation with D03 kind is called flash operation. It creates a flash object by replicating the
 * current aperture at the operation coordinates.
 */
@Getter
public class OperationD0n extends AbstractGerberCommand {
    public static final Pattern PATTERN = Pattern.compile(CoordinateData.PATTERN.pattern() + "D(\\d+)");
    public static final GerberCommandParser PARSER = new RegexpCommandParser(PATTERN, OperationD0n::new);
    private final OperationKind kind;

    public enum OperationKind {
        INTERPOLATE(1),
        MOVE(2),
        FLASH(3);

        private final int code;

        OperationKind(int code) {
            this.code = code;
        }
    }
    private final CoordinateData coordinates;

    public OperationD0n(OperationKind kind, CoordinateData coordinates) {
        super(null);
        this.kind = kind;
        this.coordinates = coordinates;
    }

    public OperationD0n(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand);

        coordinates = new CoordinateData(matcher.group(1));

        this.kind = dCodeToKind(matcher.group(2));
        if (kind == null) {
            DataBlock dataBlock = extendedCommand.getDatablocks().get(0);
            throw new ParseException(dataBlock.getLocation(), "Unknown command code: "+dataBlock.getContent());
        }
    }

    public static OperationKind dCodeToKind(String dCode) {
        int code = Integer.parseInt(dCode);
        if (code == OperationKind.INTERPOLATE.code) {
            return OperationKind.INTERPOLATE;
        } else if (code == OperationKind.MOVE.code) {
            return OperationKind.MOVE;
        } else if (code == OperationKind.FLASH.code) {
            return OperationKind.FLASH;
        } else {
            return null;
        }
    }

    @Override
    public String getGerberString() {
        return coordinates.getGerberString()+String.format("D%02d*", kind.code);
    }

    public OperationD0n transform(GerberFile targetGerberFile, GerberTransform transform) {
        return new OperationD0n(kind, coordinates.transform(getGerberFile(), targetGerberFile, transform));
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        targetGerberFile.append(transform(targetGerberFile, transform));
    }

    @Override
    public void render(GerberRenderTarget renderTarget) {
        renderTarget.operation(this);
    }
}
