package dk.dren.jerber.parser.cmd;

import dk.dren.jerber.parser.GerberFile;
import dk.dren.jerber.parser.lowlevel.DataBlock;
import dk.dren.jerber.parser.lowlevel.ExtendedCommand;
import dk.dren.jerber.parser.lowlevel.ParseException;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import dk.dren.jerber.parser.render.RenderTarget;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 7.4 Using G01/G02/G03 in a data block with D01/D02
 * This construction is deprecated since revision 2015.06.
 * The function codes G01, G02, G03 could be put together with operation codes in the same data
 * block. The graphics state is then modified before the operation coded is executed, whatever the
 * order of the codes.
 */
@Getter
public class LegacyOperationG0nD0n extends AbstractGerberCommand {
    public static final GerberCommandParser PARSER = new RegexpCommandParser(
            Pattern.compile("G(\\d+)"+
            "("+CoordinateData.PATTERN.pattern()+
            "D(\\d+))"), LegacyOperationG0nD0n::new);
    private final InterpolationModeG0n gCommand;
    private final OperationD0n operationCommand;

    public LegacyOperationG0nD0n(Matcher matcher, ExtendedCommand extendedCommand) {
        super(extendedCommand);

        gCommand = new InterpolationModeG0n(matcher, extendedCommand);

        Matcher dnm = OperationD0n.PATTERN.matcher(matcher.group(2));
        if (!dnm.matches()) {
            DataBlock dataBlock = extendedCommand.getDatablocks().get(0);
            throw new ParseException(getLocation(), "Could not parse the operation part of the command: "+dataBlock.getContent());
        }
        operationCommand = new OperationD0n(dnm, extendedCommand);
    }

    public LegacyOperationG0nD0n(InterpolationModeG0n gCommand, OperationD0n operationCommand) {
        super(null);
        this.gCommand = gCommand;
        this.operationCommand = operationCommand;
    }

    @Override
    public String getGerberString() {
        return String.format("G%02d%s", gCommand.getGCode(), operationCommand.getGerberString());
    }

    @Override
    public void append(GerberFile targetGerberFile, GerberTransform transform) {
        targetGerberFile.append(new LegacyOperationG0nD0n(gCommand, operationCommand.transform(targetGerberFile, transform)));
    }

    @Override
    public void setGerberFile(GerberFile gerberFile) {
        operationCommand.setGerberFile(gerberFile);
    }

    @Override
    public void render(GerberRenderTarget renderTarget) {
        gCommand.render(renderTarget);
        operationCommand.render(renderTarget);
    }
}
