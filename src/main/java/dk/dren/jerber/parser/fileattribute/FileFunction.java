package dk.dren.jerber.parser.fileattribute;

import dk.dren.jerber.parser.cmd.FileAttributesTF;
import dk.dren.jerber.parser.cmd.GerberCommand;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
public class FileFunction implements Comparable<FileFunction> {
    public static final String NAME = ".FileFunction";
    private static final Pattern LAYER = Pattern.compile("L(\\d+)");
    public static final String COPPER = "Copper";
    public static final String PROFILE = "Profile";

    private final String type;
    private final int copperLayer;
    private final List<String> attributes;

    public FileFunction(FileAttributesTF ff) {
        if (!ff.getName().equals(FileFunction.NAME)) {
            throw new IllegalArgumentException("File attribute is "+ff);
        }

        this.attributes = new ArrayList<>(ff.getValues());
        this.type = attributes.get(0);

        if (type.equals(COPPER)) {
            Matcher layerMatch = LAYER.matcher(attributes.get(1));
            if (layerMatch.matches()) {
                this.copperLayer = Integer.parseInt(layerMatch.group(1));
            } else {
                throw new IllegalArgumentException("The copper layer looks strange: "+attributes.get(1));
            }
        } else {
            copperLayer = 0;
        }
    }

    public GerberCommand toGerberCommand() {
        return new FileAttributesTF(FileFunction.NAME, attributes);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileFunction that = (FileFunction) o;
        return type.equals(that.type) &&
                attributes.equals(that.attributes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, attributes);
    }

    @Override
    public String toString() {
        return String.join(",", attributes);
    }

    @Override
    public int compareTo(FileFunction o) {
        if (equals(o)) {
            return 0;
        }

        int tc = type.compareTo(o.type);
        if (tc != 0) {
            return tc;
        }

        if (type.equals(COPPER)) {
            return Integer.compare(copperLayer, o.copperLayer);
        } else {
            return toString().compareTo(o.toString());
        }
    }

    public String getFileName() {
        return String.join("-", attributes)+".gbr";
    }
}
