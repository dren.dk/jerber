package dk.dren.jerber.parser;

import dk.dren.jerber.parser.cmd.*;
import dk.dren.jerber.parser.fileattribute.FileFunction;
import dk.dren.jerber.parser.geometry.BoundingBox;
import dk.dren.jerber.parser.geometry.Point6f;
import dk.dren.jerber.parser.lowlevel.ParseException;
import dk.dren.jerber.parser.lowlevel.GerberFileParser;
import dk.dren.jerber.parser.render.BoundingBoxRenderTarget;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import dk.dren.jerber.parser.util.UnitConverter;
import lombok.Getter;
import lombok.extern.java.Log;
import org.decimal4j.immutable.Decimal6f;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Reads a gerber file into memory and keeps it there.
 */
@Log
@Getter
public class GerberFile {
    private final String fileName;
    private FormatSpecificationFSLA format;
    private UnitMO unit;
    private Map<String, FileAttributesTF> fileAtributeByName = new TreeMap<>();

    private final List<GerberCommand> commands;

    private final List<ApertureDefinitionAD> apertureDefinitions;
    private TreeMap<String, Integer> apertureDefinitionToDCode = new TreeMap<>();;
    private TreeMap<Integer, String> apertureDCodeToDefinition = new TreeMap<>();;
    private int apertureDefinitionMaxDCode = 1000;
    private BoundingBox boundingBox;


    public GerberFile(String fileName, InputStreamReader is) throws IOException {
        this.fileName = fileName;
        GerberFileParser parser = new GerberFileParser(fileName, is);

        apertureDefinitions = new ArrayList<>();
        commands = new ArrayList<>();
        BoundingBoxRenderTarget bbrt = new BoundingBoxRenderTarget();
        GerberRenderTarget gerberRenderTarget = new GerberRenderTarget(bbrt);
        while (parser.hasNext()) {
            GerberCommand cmd = parser.next();
            cmd.render(gerberRenderTarget);
            append(cmd);
        }
        this.boundingBox = bbrt.getBoundingBox();
        log.info("Loaded "+fileName+" found "+ commands.size()+" gerber commands, metric="+unit.isMetric());
    }

    /**
     * Creates a new, empty gerber file with nm resolution.
     */
    public GerberFile(String fileName) {
        this.fileName = fileName;
        apertureDefinitions = new ArrayList<>();
        commands = new ArrayList<>();
        boundingBox = BoundingBox.createUnset();
        append(UnitMO.createMetric());
        append(FormatSpecificationFSLA.createNanoMeter());
    }

    public void append(GerberCommand command) {
        boolean freshCommand = command.getGerberFile() == null;
        command.setGerberFile(this);

        if (command instanceof UnitMO) {
            if (unit != null) {
                throw new ParseException(command.getLocation(), "Unit has already been set");
            }
            unit = (UnitMO)command;

        } else if (command instanceof FormatSpecificationFSLA) {
            if (format != null) {
                throw new ParseException(command.getLocation(), "Format has already been set");
            }
            format = (FormatSpecificationFSLA)command;

        } else if (command instanceof ApertureDefinitionAD) {
            ApertureDefinitionAD ad = (ApertureDefinitionAD) command;
            if (freshCommand) {
                registerAperture(ad.getDefinition(), ad.getDCode());
                apertureDefinitions.add(ad);
            } else {
                Integer oldDCode = getDCodeForApertureDefinition(ad.getDefinition());
                if (oldDCode == null) {
                    // Need to add it, so allocate a dCode
                    int newDCode = allocateApertureDCode(ad.getDefinition(), ad.getDCode());
                    if (newDCode != ad.getDCode()) {
                        command = new ApertureDefinitionAD(newDCode, ad.getTemplate(), ad.getModifiers());
                        command.setGerberFile(this);
                    }

                    apertureDefinitions.add((ApertureDefinitionAD) command);
                }
            }

        } else if (command instanceof FileAttributesTF) {
            FileAttributesTF fa = (FileAttributesTF) command;
            fileAtributeByName.put(fa.getName(), fa);

        } else {
            commands.add(command);
        }
    }

    public static GerberFile load(File file) throws IOException {
        try (FileReader fr = new FileReader(file)) {
            return new GerberFile(file.getAbsolutePath(), fr);
        }
    }

    public void store(File file) throws IOException {
        try (Writer writer = new FileWriter(file)) {
            store(writer);
        }
    }

    public void store(Writer writer) throws IOException {

        for (FileAttributesTF fileAttributeTF : fileAtributeByName.values()) {
            writer.append(fileAttributeTF.getGerberString());
            writer.append("\n");
        }
        writer.append(unit.getGerberString());
        writer.append("\n");
        writer.append(format.getGerberString());
        writer.append("\n");

        for (GerberCommand command : apertureDefinitions) {
            writer.append(command.getGerberString());
            writer.append("\n");
        }

        for (GerberCommand command : commands) {
            writer.append(command.getGerberString());
            writer.append("\n");
        }

        writer.append(new EndOfFileM02().getGerberString());
        writer.append("\n");
        writer.flush();
    }

    public void append(GerberFile source, GerberTransform transform) {
        append(new CommentG04("--- Start of "+source.getFileName()+" @ "+transform));
        source.getApertureDefinitions().forEach(c->c.append(this, transform));
        source.getCommands().forEach(c->c.append(this, transform));
        append(new CommentG04("--- End of "+source.getFileName()+" @ "+transform));
    }

    public GerberTransform offset(Decimal6f xOffsetInMilimeter, Decimal6f yOffsetInMilimeter) {
        return new GerberTransform(
                getXConverter(this).mmToTargetUnscaled(xOffsetInMilimeter),
                getYConverter(this).mmToTargetUnscaled(yOffsetInMilimeter));
    }

    public int allocateApertureDCode(String definition, Integer requestedDCode) {
        Integer existingDCode = getDCodeForApertureDefinition(definition);
        if (existingDCode != null) {
            return existingDCode;
        }

        int dCode;
        if (requestedDCode == null || getApertureDefinitionForDCode(requestedDCode) != null) {
            dCode = apertureDefinitionMaxDCode++;
        } else {
            dCode = requestedDCode;
            if (dCode > apertureDefinitionMaxDCode) {
                apertureDefinitionMaxDCode = dCode+1;
            }
        }

        registerAperture(definition, dCode);

        return dCode;
    }

    private void registerAperture(String definition, int dCode) {
        apertureDefinitionToDCode.put(definition, dCode);
        apertureDCodeToDefinition.put(dCode, definition);
    }

    public Integer getDCodeForApertureDefinition(String definition) {
        return apertureDefinitionToDCode.get(definition);
    }

    public String getApertureDefinitionForDCode(int dCode) {
        return apertureDCodeToDefinition.get(dCode);
    }

    public UnitConverter getXConverter(GerberFile targetGerberFile) {
        return new UnitConverter(getUnit().isMetric(), getFormat().getXDecimalDigits(),
                targetGerberFile.getUnit().isMetric(), targetGerberFile.getFormat().getXDecimalDigits());
    }

    public UnitConverter getYConverter(GerberFile targetGerberFile) {
        return new UnitConverter(getUnit().isMetric(), getFormat().getYDecimalDigits(),
                targetGerberFile.getUnit().isMetric(), targetGerberFile.getFormat().getYDecimalDigits());
    }

    public FileFunction getFileFunction() {
        FileAttributesTF fa = fileAtributeByName.get(FileFunction.NAME);
        if (fa == null) {
            return null;
        }
        return new FileFunction(fa);
    }

    public void render(GerberRenderTarget gerberRenderTarget) {
        unit.render(gerberRenderTarget);
        format.render(gerberRenderTarget);

        for (GerberCommand command : apertureDefinitions) {
            command.render(gerberRenderTarget);
        }

        for (GerberCommand command : commands) {
            command.render(gerberRenderTarget);
        }
    }
}
