package dk.dren.jerber.parser.util;

import org.decimal4j.base.AbstractImmutableDecimal;
import org.decimal4j.immutable.Decimal6f;

/**
 * Formats a fixed point decimal number without garbage zeroes at the end.
 */
public class NiceDecimal {
    public static String format(AbstractImmutableDecimal d) {
        String withTrailingZeroes = d.toString();
        if (!withTrailingZeroes.contains(".")) {
            return withTrailingZeroes; // No decimal point...
        }

        // Get rid of all trailing zeroes after the decimal point and also the decimal point if it ends up being
        // last
        return withTrailingZeroes.replaceAll("0+$", "").replaceAll("\\.$", "");
    }

    public static String formatWithDot(Decimal6f d) {
        String withTrailingZeroes = d.toString();
        if (!withTrailingZeroes.contains(".")) {
            return withTrailingZeroes+"."; // No decimal point...
        }

        // Get rid of all trailing zeroes after the decimal point, but not the decimal point
        return withTrailingZeroes.replaceAll("0+$", "");
    }
}
