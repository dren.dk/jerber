package dk.dren.jerber.parser.util;

import dk.dren.jerber.parser.cmd.UnitMO;
import lombok.Getter;
import org.decimal4j.immutable.Decimal6f;

/**
 * Converts from one unit and decimal places to another unit and decimal places
 */
@Getter
public class UnitConverter {
    private final boolean fromMetric;
    private final int fromDecimals;
    private final boolean toMetric;
    private final int toDecimals;

    private static final Decimal6f MM_PER_INCH = Decimal6f.valueOf("25.4");
    private final Decimal6f longsPerFromUnit;
    private final Decimal6f fromUnitsInMiliMeter;
    private final Decimal6f toUnitsInMilimeter;
    private final Decimal6f longsPerToUnit;
    private final Decimal6f conversionFactor;

    public UnitConverter(UnitMO fromUnit, UnitMO toUnit) {
        this(fromUnit.isMetric(), 6, toUnit.isMetric(), 6);
    }

    public UnitConverter(boolean fromMetric, int fromDecimals, boolean toMetric, int toDecimals) {
        this.fromMetric = fromMetric;
        this.fromDecimals = fromDecimals;
        this.toMetric = toMetric;
        this.toDecimals = toDecimals;

        longsPerFromUnit = Decimal6f.ONE.divideByPowerOfTen(fromDecimals);
        fromUnitsInMiliMeter = fromMetric ? Decimal6f.ONE : MM_PER_INCH;
        toUnitsInMilimeter   = toMetric   ? Decimal6f.ONE : MM_PER_INCH;
        longsPerToUnit  = Decimal6f.ONE.divideByPowerOfTen(toDecimals);

        conversionFactor = Decimal6f.ONE.divideByPowerOfTen(fromDecimals-toDecimals).multiply(fromUnitsInMiliMeter).divide(toUnitsInMilimeter);
    }

    /**
     * Converts from unscaledToUnscaled units to unscaledToUnscaled units
     * @param unscaled The number of unscaledToUnscaled units of fromUnit with fromDecimals
     * @return The number of unscaledToUnscaled untis of toUnit with toDecimals
     */
    public long unscaledToUnscaled(long unscaled) {
        return Decimal6f.valueOf(unscaled).multiply(conversionFactor).longValue();
    }

    public Decimal6f scaledToScaled(Decimal6f from) {
        return from.multiply(fromUnitsInMiliMeter).divide(toUnitsInMilimeter);
    }

    public long mmToTargetUnscaled(Decimal6f mm) {
        return mm.divide(toUnitsInMilimeter).divide(longsPerToUnit).longValue();
    }

    public Decimal6f unscaledTomm(long unscaled) {
        return Decimal6f.valueOf(unscaled).multiply(longsPerFromUnit).multiply(fromUnitsInMiliMeter);
    }
}
