package dk.dren.jerber.parser;

import dk.dren.jerber.parser.drillcmd.Comment;
import dk.dren.jerber.parser.drillcmd.FormatVersionFMAT;
import dk.dren.jerber.parser.drillcmd.Metric;
import dk.dren.jerber.parser.drillcmd.StartHeaderM48;
import dk.dren.jerber.parser.fileattribute.FileFunction;
import dk.dren.jerber.parser.geometry.BoundingBox;
import dk.dren.jerber.parser.render.GerberRenderTarget;
import dk.dren.jerber.parser.render.SvgLayerRenderTarget;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.decimal4j.immutable.Decimal6f;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

@RequiredArgsConstructor
@Getter
public class GerberFabricationSet {
    private final String name;
    private Map<FileFunction, GerberFile> fileByFunction = new TreeMap<>();
    private Map<DrillFile.DrillType, DrillFile> drillFiles = new TreeMap<>();
    private BoundingBox boundingBox = BoundingBox.createUnset();

    public static GerberFabricationSet loadZip(File zip) throws IOException {

        GerberFabricationSet res = new GerberFabricationSet(zip.getAbsolutePath());
        try (ZipFile z = new ZipFile(zip)) {
            z.stream().filter(e -> e.getName().endsWith(".gbr")).forEach(zipEntry -> {
                String entryName = zip.getName() + "#" + zipEntry.getName();
                try (InputStreamReader is = new InputStreamReader(z.getInputStream(zipEntry), StandardCharsets.UTF_8)) {
                    GerberFile gf = new GerberFile(entryName, is);
                    FileFunction fileFunction = gf.getFileFunction();
                    if (fileFunction == null) {
                        throw new IllegalArgumentException("The gerber file " + entryName + " did not contain a FileFunction attribute, cannot automagically load from zip");
                    }

                    res.addFile(fileFunction, gf);
                } catch (IOException e) {
                    throw new RuntimeException("Failed while reading " + entryName, e);
                }
            });

            z.stream().filter(e -> e.getName().endsWith(".drl")).forEach(zipEntry -> {
                String entryName = zip.getName() + "#" + zipEntry.getName();
                try (InputStreamReader is = new InputStreamReader(z.getInputStream(zipEntry), StandardCharsets.UTF_8)) {
                    DrillFile dr = new DrillFile(entryName, is);
                    DrillFile.DrillType type;
                    if (zipEntry.getName().endsWith("-PTH.drl")) {
                        type = DrillFile.DrillType.PLATED;

                    } else if (zipEntry.getName().endsWith("-NPTH.drl")) {
                        type = DrillFile.DrillType.NON_PLATED;

                    } else {
                        throw new IllegalArgumentException("The drill file " + entryName + " did not look like either a -PTH.drl or a -NPTH.drl, cannot automagically load from zip");
                    }
                    res.addFile(type, dr);
                } catch (IOException e) {
                    throw new RuntimeException("Failed while reading " + entryName, e);
                }
            });
        }

        return res;
    }

    private void addFile(DrillFile.DrillType type, DrillFile drillFile) {
        drillFiles.put(type, drillFile);
    }

    public void addFile(FileFunction fileFunction, GerberFile gf) {
        fileByFunction.put(fileFunction, gf);
        boundingBox = boundingBox.union(gf.getBoundingBox());
    }

    public void store(File zip) throws IOException {
        try (OutputStream os = new FileOutputStream(zip);
             ZipOutputStream zos = new ZipOutputStream(os)) {

            for (Map.Entry<FileFunction, GerberFile> functionAndFile : fileByFunction.entrySet()) {

                ZipEntry entry = new ZipEntry(name+"/"+name+"-"+functionAndFile.getKey().getFileName());
                zos.putNextEntry(entry);
                OutputStreamWriter writer = new OutputStreamWriter(zos);
                functionAndFile.getValue().store(writer);
                writer.flush();
                zos.flush();
                zos.closeEntry();
                zos.flush();
            }
        }
    }

    public BoundingBox getBoundingBox() {
        return boundingBox;
    }

    public void renderSvg(File file) throws IOException {
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file))) {
            renderSvg(writer);
        }
    }

    private void renderSvg(OutputStreamWriter writer) throws IOException {
        IOUtils.copy(GerberFabricationSet.class.getResourceAsStream("/template.svg"), writer, "UTF-8");
        int layerNumber = 1;
        for (GerberFile gerberFile : fileByFunction.values()) {
            try (SvgLayerRenderTarget svgLayerRenderer = new SvgLayerRenderTarget(writer, gerberFile.getFileFunction().getFileName(), layerNumber++)) {

                if (gerberFile.getFileFunction().getType().equals("Profile")) {
                    svgLayerRenderer.setForceStyle("opacity:1; stroke:black; stroke-linecap:round; stroke-linejoin:round; stroke-width:0.15");
                }

                gerberFile.render(new GerberRenderTarget(svgLayerRenderer));
            }
        }

        for (Map.Entry<DrillFile.DrillType, DrillFile> typeAndDrill : drillFiles.entrySet()) {
            DrillFile.DrillType drillType = typeAndDrill.getKey();
            DrillFile drillFile = typeAndDrill.getValue();
            try (SvgLayerRenderTarget svgLayerRenderer = new SvgLayerRenderTarget(writer, drillFile.getFileName(), layerNumber++)) {
                drillFile.render(svgLayerRenderer);
            }
        }

        writer.append("</svg>");
    }

    public void append(GerberFabricationSet source, Decimal6f xOffset, Decimal6f yOffset) {

        for (GerberFile sourceFile : source.getFileByFunction().values()) {
            GerberFile targetFile = fileByFunction.computeIfAbsent(sourceFile.getFileFunction(), this::createGerberLayer);
            targetFile.append(sourceFile, targetFile.offset(xOffset, yOffset));
        }

        for (Map.Entry<DrillFile.DrillType, DrillFile> sourceDrill : source.getDrillFiles().entrySet()) {
            DrillFile targetDrill = drillFiles.computeIfAbsent(sourceDrill.getKey(), this::createDrillLayer);
            targetDrill.append(sourceDrill.getValue(), xOffset, yOffset);
        }
    }

    private DrillFile createDrillLayer(DrillFile.DrillType type) {
        return new DrillFile(name+"-"+type.name()+".drl");
    }

    private GerberFile createGerberLayer(FileFunction ff) {
        GerberFile target = new GerberFile(name+"-"+ff.getFileName());

        target.append(ff.toGerberCommand());
        return target;
    }
}
