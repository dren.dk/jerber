package dk.dren.jerber.parser.lowlevel;

import dk.dren.jerber.parser.cmd.*;
import lombok.RequiredArgsConstructor;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RequiredArgsConstructor
public class GerberFileParser implements Closeable, Iterator<GerberCommand> {
    private final GerberTokenizer tokenizer;

    public GerberFileParser(String fileName, InputStreamReader inputStreamReader) throws IOException {
        this(new GerberTokenizer(fileName, inputStreamReader));
    }

    private static final List<GerberCommandParser> CMDS;
    static {
        CMDS = new ArrayList<>();
        CMDS.add(CommentG04.PARSER);
        CMDS.add(FormatSpecificationFSLA.PARSER);
        CMDS.add(UnitMO.PARSER);
        CMDS.add(FileAttributesTF.PARSER);
        CMDS.add(LoadPolarityLP.PARSER);
        CMDS.add(ApertureDefinitionAD.PARSER);
        CMDS.add(SetCurrentApertureDnn.PARSER);
        CMDS.add(OperationD0n.PARSER);
        CMDS.add(InterpolationModeG0n.PARSER);
        CMDS.add(EndOfFileM02.PARSER);
        CMDS.add(ApertureMacroAM.PARSER);
        CMDS.add(InterpolationModeG0n.PARSER);
        CMDS.add(QuadrantModeG7n.PARSER);
        CMDS.add(RegionBeginG36.PARSER);
        CMDS.add(RegionEndG37.PARSER);
        CMDS.add(ApertureAttributeTA.PARSER);
        CMDS.add(DeleteAttributeTD.PARSER);
        CMDS.add(ObjectAttributesTO.PARSER);
        CMDS.add(LegacyOperationG0nD0n.PARSER);
    }

    @Override
    public void close() throws IOException {
        tokenizer.close();
    }

    @Override
    public boolean hasNext() {
        return tokenizer.hasNext();
    }

    @Override
    public GerberCommand next() {
        ExtendedCommand ec = tokenizer.next();
        DataBlock firstDataBlock = ec.getDatablocks().get(0);
        for (GerberCommandParser commandParser : CMDS) {
            GerberCommand gerberCommand = commandParser.tryParse(ec);
            if (gerberCommand != null) {
                return gerberCommand;
            }
        }
        throw new ParseException(firstDataBlock.getLocation(), "Unrecognized command: "+firstDataBlock.toString());
    }
}
