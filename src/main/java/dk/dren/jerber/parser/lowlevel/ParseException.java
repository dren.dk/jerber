package dk.dren.jerber.parser.lowlevel;

import lombok.RequiredArgsConstructor;

import java.io.IOException;

@RequiredArgsConstructor
public class ParseException extends RuntimeException {
    public ParseException(FileLocation location, String msg, IOException e) {
        super("Parsing failed "+ getLocationText(location) +": "+msg, e);
    }

    private static String getLocationText(FileLocation location) {
        return location == null ? "at unknown location" : location.toString();
    }

    public ParseException(FileLocation location, String msg) {
        this(location, msg, null);
    }
}
