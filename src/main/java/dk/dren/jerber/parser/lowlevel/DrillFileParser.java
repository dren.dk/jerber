package dk.dren.jerber.parser.lowlevel;

import dk.dren.jerber.parser.DrillFile;
import dk.dren.jerber.parser.drillcmd.*;
import lombok.RequiredArgsConstructor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Parses Excellon files and appends all the commands to a DrillFile
 */
@RequiredArgsConstructor
public class DrillFileParser {
    private static final List<DrillCommandParser> PARSERS;
    static {
        PARSERS = new ArrayList<>();
        PARSERS.add(StartHeaderM48.PARSER);
        PARSERS.add(Comment.PARSER);
        PARSERS.add(FormatVersionFMAT.PARSER);
        PARSERS.add(Metric.PARSER);
        PARSERS.add(ToolDefinition.PARSER);
        PARSERS.add(RewindStop.PARSER);
        PARSERS.add(AbsoluteModeG90.PARSER);
        PARSERS.add(DrillModeG05.PARSER);
        PARSERS.add(SelectToolT.PARSER);
        PARSERS.add(EndOfProgramM30.PARSER);
        PARSERS.add(DrillHitXY.PARSER);
    }

    private final String fileName;
    private final InputStreamReader is;
    private final DrillFile drillFile;
    private int lineNumber = 1;

    public void parse() {
        BufferedReader bufferedReader = new BufferedReader(is);
        bufferedReader.lines().forEach(this::addLine);
    }

    private void addLine(String line) {
        line = line.trim();
        FileLocation location = new FileLocation(fileName, lineNumber++);
        for (DrillCommandParser parser : PARSERS) {
            DrillCommand drillCommand = parser.tryParse(location, line);
            if (drillCommand != null) {
                drillFile.append(drillCommand);
                return;
            }
        }
        throw new ParseException(location, "Unable to parse '"+line+"'");
    }
}
