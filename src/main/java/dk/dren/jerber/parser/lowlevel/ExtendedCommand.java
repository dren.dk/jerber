package dk.dren.jerber.parser.lowlevel;

import lombok.Value;

import java.util.List;
import java.util.stream.Collectors;

@Value
public class ExtendedCommand {
    private final List<DataBlock> datablocks;

    @Override
    public String toString() {
        if (datablocks.size() > 1) {
            return "%"+ datablocks.stream().map(DataBlock::toString).collect(Collectors.joining("\n"))+"%";
        } else {
            return datablocks.get(0).toString();
        }
    }
}
