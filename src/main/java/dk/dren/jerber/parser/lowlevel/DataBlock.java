package dk.dren.jerber.parser.lowlevel;

import lombok.Value;

@Value
public class DataBlock {
    FileLocation location;
    String content;

    @Override
    public String toString() {
        return content + '*';
    }
}
