package dk.dren.jerber.parser.lowlevel;

import dk.dren.jerber.parser.drillcmd.DrillCommand;
import dk.dren.jerber.parser.drillcmd.ToolDefinition;
import org.decimal4j.immutable.Decimal6f;

import java.io.Writer;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The drill rack, aka. the tool box aka the drill index that maps a drill number to a drill size and the other way around
 */
public class DrillIndex {
    private final NavigableMap<Decimal6f, Integer> drillSizeToNumber = new TreeMap<>();
    private final NavigableMap<Integer, Decimal6f> drillNumberToSize = new TreeMap<>();

    public Decimal6f getDrillSizeForNumber(int drillNumber) {
        return drillNumberToSize.get(drillNumber);
    }

    public Integer getDrillNumberForSize(Decimal6f drillSize) {
        return drillSizeToNumber.get(drillSize);
    }

    /**
     * Adds a drill to the register, preferably with the same number as the original command, but that's not guaranteed
     *
     * @param td the tool definition with the size and the requested tool number
     * @return The actual tool number assigned.
     */
    public Integer addDrill(ToolDefinition td) {
        final Decimal6f size = td.getSize();
        final Integer existingNumber = getDrillNumberForSize(size);
        if (existingNumber == null) {
            final Decimal6f existingSize = getDrillSizeForNumber(td.getNumber());
            final Integer number;
            if (existingSize == null) {
                number = td.getNumber(); // The requested number was free, so use that.
            } else {
                number = drillNumberToSize.lastKey()+1; // The requested number was taken, so add one to the last one already registered and use that.
            }
            addDrill(number, size);
            return number;
        } else {
            return existingNumber;
        }
    }

    public Set<Map.Entry<Integer, Decimal6f>> getAllDrills() {
        return drillNumberToSize.entrySet();
    }

    private void addDrill(Integer number, Decimal6f size) {
        drillNumberToSize.put(number, size);
        drillSizeToNumber.put(size, number);
    }

    public List<DrillCommand> commands() {
        return drillNumberToSize.entrySet().stream()
                .map(e->new ToolDefinition(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }
}
