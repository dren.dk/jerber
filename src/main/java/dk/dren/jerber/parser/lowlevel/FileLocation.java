package dk.dren.jerber.parser.lowlevel;

import lombok.Value;

@Value
public class FileLocation {
    private final String fileName;
    private final Integer line;

    @Override
    public String toString() {
        return fileName + '(' +line +')';
    }
}
