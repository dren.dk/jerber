package dk.dren.jerber.parser.lowlevel;

import lombok.Getter;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Reads commands from the gerber file using single-symbol-lookahead and returns each command/extended command
 * found in the file.
 *
 * The purpose is to normalize the input into the datablocks it consists of.
 *
 * I'm cheating a bit here because there are no symbols longer than a single char
 */
@Getter
public class GerberTokenizer implements Closeable, Iterator<ExtendedCommand> {
    private static final int EOF = -1;
    private static final char EXTENDED_COMMAND_DELIMITER = '%';
    private static final char DATA_BLOCK_DELIMITER = '*';
    private final InputStreamReader reader;
    private final String fileName;
    private int line;
    private int nextChar;

    public FileLocation getLocation() {
        return new FileLocation(fileName, line);
    }

    public GerberTokenizer(String fileName, InputStreamReader reader) throws IOException {
        this.fileName = fileName;
        this.reader = reader;
        line = 0;
        readChar();
    }

    private void readSingleChar() {
        try {
            nextChar = reader.read();
        } catch (IOException e) {
            throw new ParseException(getLocation(), "Failed to read next char", e);
        }
        if (nextChar == '\n') {
            line++;
        }
    }

    private void readChar() {
        do {
            readSingleChar();
        } while (nextChar != EOF && (nextChar == '\n' || nextChar == '\r'));
    }

    @Override
    public boolean hasNext() {
        return nextChar != EOF;
    }

    /**
     * Reads an extended command section from the reader
     *
     * @return The next command or null at eof
     */
    @Override
    public ExtendedCommand next() {
        if (!hasNext()) {
            return null;
        }

        if (nextChar == EXTENDED_COMMAND_DELIMITER) {
            return readExtendedCommand();
        } else {
            return readFunctionalCodeCommand();
        }
    }

    private ExtendedCommand readFunctionalCodeCommand() {
        List<DataBlock> blocks = new ArrayList<>();
        blocks.add(readDataBlock());
        return new ExtendedCommand(blocks);
    }

    private ExtendedCommand readExtendedCommand() {
        readChar();
        List<DataBlock> blocks = new ArrayList<>();
        while (nextChar != EOF) {
            blocks.add(readDataBlock());
            if (nextChar == EXTENDED_COMMAND_DELIMITER) {
                readChar();
                return new ExtendedCommand(blocks);
            }
        }
        throw new ParseException(getLocation(), "Did not find ending "+EXTENDED_COMMAND_DELIMITER+" before EOF");
    }

    private DataBlock readDataBlock() {
        int startLine = line;
        StringBuilder sb = new StringBuilder();
        while (nextChar != EOF) {
            sb.append((char)nextChar);
            readChar();
            if (nextChar == DATA_BLOCK_DELIMITER) {
                readChar();
                return new DataBlock(getLocation(), sb.toString().trim());
            }
        }
        throw new ParseException(getLocation(), "Did not find "+DATA_BLOCK_DELIMITER+" before EOF");
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }
}
