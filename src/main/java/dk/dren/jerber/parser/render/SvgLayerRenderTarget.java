package dk.dren.jerber.parser.render;

import dk.dren.jerber.parser.geometry.Point6f;
import dk.dren.jerber.parser.util.NiceDecimal;
import lombok.Setter;
import lombok.extern.java.Log;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

@Log
public class SvgLayerRenderTarget implements RenderTarget, Closeable {
    private final OutputStreamWriter writer;
    Point6f pos;

    private boolean dark;
    private Aperture aperture;
    List<String> path = new ArrayList<>();
    @Setter
    String forceStyle;
    private static long idCounter = 1;

    public SvgLayerRenderTarget(OutputStreamWriter writer, String name, int layerNumber) throws IOException {
        this.writer = writer;
        writer.append("  <g inkscape:label=\""+name+"\" inkscape:groupmode=\"layer\" id=\"layer"+layerNumber+"\" style=\"fill:none\">\n");
    }

    /**
     * Emit the path buffered in the path list with the style defined by dark and aperture
     */
    private void flushPath() {
        if (path.isEmpty()) {
            return;
        }

        String style = "opacity:1; ";
        if (dark) {
            style += "stroke:black; ";
        } else {
            style += "stroke:white; ";
        }

        style += aperture.getSvgPathStyle();

        try {
            if (forceStyle != null) {
                style = forceStyle;
            }

            writer.append("        <path id=\"p-"+(idCounter++)+"\" style=\""+style+"\" d=\""+String.join(" ", path)+"\" />\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        path.clear();
    }

    @Override
    public void setPolarity(boolean dark) {
        flushPath();
        this.dark = dark;
    }

    @Override
    public void setAperture(Aperture aperture) {
        flushPath();
        this.aperture = aperture;
    }

    @Override
    public void moveTo(Point6f point) {
        flushPath();
        path.add("M "+ NiceDecimal.format(point.getX())+" "+NiceDecimal.format(point.getY().negate()));
        pos = point;
    }

    @Override
    public void drawTo(Point6f point) {
        path.add("L "+ NiceDecimal.format(point.getX())+" "+NiceDecimal.format(point.getY().negate()));
        pos = point;
    }

    @Override
    public void flash(Point6f point) {
        flushPath();

        try {
            writer.append(aperture.flashSvg(point));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        pos = point;
    }

    @Override
    public void arcTo(Point6f center, Point6f endPoint, boolean clockwise, boolean multiQuadrant) {

        String r = NiceDecimal.format(center.distance(endPoint));
//x-axis-rotation large-arc-flag sweep-flag
        path.add("A "+r+" "+r+" 0 0 1 "+
                NiceDecimal.format(endPoint.getX())+" "+NiceDecimal.format(endPoint.getY().negate()));

        pos = endPoint;
    }

    @Override
    public void regionBegin() {
        flushPath();
        try {
            writer.append("<g style=\"fill:black\"><!-- start of region -->\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void regionEnd() {
        if (path.isEmpty()) {
            throw new IllegalArgumentException("Region end without any contained path");
        }
        flushPath();
        try {
            writer.append("</g><!-- end of region -->\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws IOException {
        flushPath();
        writer.append("  </g>\n");
    }

}
