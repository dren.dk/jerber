package dk.dren.jerber.parser.render;

import dk.dren.jerber.parser.geometry.Point6f;

public interface Aperture {

    String getSvgPathStyle();
    String flashSvg(Point6f point);
}
