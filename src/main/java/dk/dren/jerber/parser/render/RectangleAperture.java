package dk.dren.jerber.parser.render;

import dk.dren.jerber.parser.geometry.Point6f;
import dk.dren.jerber.parser.util.NiceDecimal;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

import java.util.List;

@RequiredArgsConstructor
@Getter
public class RectangleAperture implements Aperture {
    private final Decimal6f width;
    private final Decimal6f height;
    private final Decimal6f holeDiameter;
    private final Decimal6f diameter;

    public RectangleAperture(List<Decimal6f> modifiers) {
        if (modifiers.size() < 2) {
            throw new IllegalArgumentException("Need at least two modifiers: X and Y size");
        }
        width = modifiers.get(0);
        height = modifiers.get(1);
        holeDiameter = modifiers.size() > 2 ? modifiers.get(2) : null;
        diameter = width.avg(height);
    }

    @Override
    public String getSvgPathStyle() {
        return "stroke-linecap:square; stroke-linejoin:miter; stroke-width:"+ NiceDecimal.format(diameter);
    }

    @Override
    public String flashSvg(Point6f point) {
        return "<rect " +
                "x=\""+NiceDecimal.format(point.getX().subtract(width.divide(2)))+"\" " +
                "y=\""+NiceDecimal.format(point.getY().negate().subtract(height.divide(2)))+"\" " +
                "width=\""+NiceDecimal.format(width)+"\" " +
                "height=\""+NiceDecimal.format(height)+"\" " +
                "style=\"fill:black;stroke:none;\"/>\n";
    }
}
