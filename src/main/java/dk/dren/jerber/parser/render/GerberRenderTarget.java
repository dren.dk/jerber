package dk.dren.jerber.parser.render;

import dk.dren.jerber.parser.cmd.ApertureDefinitionAD;
import dk.dren.jerber.parser.cmd.CoordinateData;
import dk.dren.jerber.parser.cmd.OperationD0n;
import dk.dren.jerber.parser.geometry.Point6f;
import dk.dren.jerber.parser.util.UnitConverter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * A class that adapts from the complicated gerber world to the more general RenderTarget world
 */
@Getter
@RequiredArgsConstructor
public class GerberRenderTarget {
    private final RenderTarget renderTarget;

    private UnitConverter unitConverter;
    private Boolean metric;
    private int decimals = 6;

    private Decimal6f x = Decimal6f.ZERO;
    private Decimal6f y = Decimal6f.ZERO;
    private boolean multiQuadrant = false;
    private int interpolationMode = 1;
    private Map<Integer, ApertureDefinitionAD> apertureByD = new TreeMap<>();

    public void setMetric(boolean metric) {
        if (this.metric != null && this.metric != metric) {
            throw new IllegalArgumentException("Cannot change unit");
        }
        this.metric = metric;
        initConverter();
    }

    public void setFixedDecimals(int decimals) {
        this.decimals = decimals;
        if (metric != null) {
            initConverter();
        }
    }

    private void initConverter() {
        // Set up the converter from whatever is in the gerber file to mm with nm resolution
        unitConverter = new UnitConverter(metric, decimals, true, 6);
    }

    public void setInterpolationMode(int interpolationMode) {
        if (interpolationMode < 1 || interpolationMode > 3) {
            throw new IllegalArgumentException("Cannot set interpolation mode < 1 or > 3, refusing: "+interpolationMode);
        }
        this.interpolationMode = interpolationMode;
    }

    public void setQuadrantMode(boolean multiQuadrant) {
        this.multiQuadrant = multiQuadrant;
    }

    public void operation(OperationD0n op) {
        CoordinateData coordinates = op.getCoordinates();

        boolean move = op.getKind().equals(OperationD0n.OperationKind.MOVE);
        boolean flash = op.getKind().equals(OperationD0n.OperationKind.FLASH);
        boolean draw = interpolationMode == 1 && op.getKind().equals(OperationD0n.OperationKind.INTERPOLATE);
        if (move || flash || draw) {
            Long cx = coordinates.get(CoordinateData.Axis.X);
            if (cx != null) {
                x = unitConverter.unscaledTomm(cx);
            } else {
                Long ci = coordinates.get(CoordinateData.Axis.I);
                if (ci != null) {
                    x = x.add(unitConverter.unscaledTomm(ci));
                }
            }

            Long cy = coordinates.get(CoordinateData.Axis.Y);
            if (cy != null) {
                y = unitConverter.unscaledTomm(cy);
            } else {
                Long cj = coordinates.get(CoordinateData.Axis.J);
                if (cj != null) {
                    y = y.add(unitConverter.unscaledTomm(cj));
                }
            }

            Point6f current = new Point6f(x, y);

            if (move) {
                renderTarget.moveTo(current);
            } else if (flash) {
                renderTarget.flash(current);
            } else {
                renderTarget.drawTo(current);
            }
        } else {
            Decimal6f centerX = x.add(unitConverter.unscaledTomm(coordinates.get(CoordinateData.Axis.I)));
            Decimal6f centerY = y.add(unitConverter.unscaledTomm(coordinates.get(CoordinateData.Axis.J)));
            x = unitConverter.unscaledTomm(coordinates.get(CoordinateData.Axis.X));
            y = unitConverter.unscaledTomm(coordinates.get(CoordinateData.Axis.Y));

            Point6f endPoint = new Point6f(x, y);
            Point6f center = new Point6f(centerX, centerY);

            renderTarget.arcTo(center, endPoint, interpolationMode == 2, multiQuadrant);
        }
    }

    public void defineAperture(ApertureDefinitionAD ad) {
        apertureByD.put(ad.getDCode(), ad);
    }

    public void setCurrentAperture(int dCode) {
        ApertureDefinitionAD ad = apertureByD.get(dCode);
        if (ad == null) {
            throw new IllegalArgumentException("Aperture D"+dCode+" not defined before use");
        }

        List<Decimal6f> modifiers = ad.getModifiers().stream()
                .map(s->unitConverter.scaledToScaled(s))
                .collect(Collectors.toList());

        if (ad.getTemplate().equals("C")) {
            renderTarget.setAperture(new CircleAperture(modifiers));
        } else if (ad.getTemplate().equals("R")) {
            renderTarget.setAperture(new RectangleAperture(modifiers));
        } else if (ad.getTemplate().equals("O")) {
            renderTarget.setAperture(new ObroundAperture(modifiers));
        } else if (ad.getTemplate().equals("P")) {
            throw new IllegalArgumentException("Polygon apertures not implemented: "+ad);

            //renderTarget.setAperture(new PolygonAperture(modifiers));
        } else {
            throw new IllegalArgumentException("Non-standard apertures not implemented: "+ad);
        }
    }

    public void setPolarity(boolean dark) {
        renderTarget.setPolarity(dark);
    }

    public void regionBegin() {
        renderTarget.regionBegin();
    }

    public void regionEnd() {
        renderTarget.regionEnd();
    }
}
