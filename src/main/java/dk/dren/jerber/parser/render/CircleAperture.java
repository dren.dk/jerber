package dk.dren.jerber.parser.render;

import dk.dren.jerber.parser.geometry.Point6f;
import dk.dren.jerber.parser.util.NiceDecimal;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

import java.util.List;

@RequiredArgsConstructor
@Getter
public class CircleAperture implements Aperture {

    private final Decimal6f diameter;
    private final Decimal6f holeDiameter;

    public CircleAperture(List<Decimal6f> modifiers) {
        if (modifiers.size() == 0) {
            throw new IllegalArgumentException("Cannot create a circle aperture without a diameter");
        } else {
            diameter = modifiers.get(0);
        }

        if (modifiers.size() > 2) {
            throw new IllegalArgumentException("Cannot create a circle aperture with more than 2 modifiers");
        } else if (modifiers.size() > 1) {
            holeDiameter = modifiers.get(1);
        } else {
            holeDiameter = null;
        }
    }

    public boolean isSolid() {
        return holeDiameter == null;
    }

    @Override
    public String getSvgPathStyle() {
        return "stroke-linecap:round; stroke-linejoin:round; stroke-width:"+ NiceDecimal.format(diameter);
    }

    @Override
    public String flashSvg(Point6f point) {
        return "<circle cx=\""+NiceDecimal.format(point.getX())+"\" cy=\""+NiceDecimal.format(point.getY().negate())+"\" " +
                "r=\""+NiceDecimal.format(diameter.divide(Decimal6f.TWO))+"\" " +
                "style=\"stroke:none; fill:black\" />\n";
    }
}
