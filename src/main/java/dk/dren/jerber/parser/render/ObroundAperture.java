package dk.dren.jerber.parser.render;

import dk.dren.jerber.parser.geometry.Point6f;
import dk.dren.jerber.parser.util.NiceDecimal;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.decimal4j.immutable.Decimal6f;

import java.util.List;

@RequiredArgsConstructor
@Getter
public class ObroundAperture implements Aperture {
    private final Decimal6f width;
    private final Decimal6f height;
    private final Decimal6f holeDiameter;

    public ObroundAperture(List<Decimal6f> modifiers) {
        if (modifiers.size() < 2) {
            throw new IllegalArgumentException("Need at least two modifiers: X and Y size");
        }
        width = modifiers.get(0);
        height = modifiers.get(1);
        holeDiameter = modifiers.size() > 2 ? modifiers.get(2) : null;
    }

    @Override
    public String getSvgPathStyle() {
        return "";
    }

    @Override
    public String flashSvg(Point6f point) {
        String r = NiceDecimal.format(width.min(height).divide(Decimal6f.TWO));
        return "<rect x=\""+NiceDecimal.format(point.getX().subtract(width.divide(Decimal6f.TWO)))+"\" " +
                "y=\""+NiceDecimal.format(point.getY().negate().subtract(height.divide(Decimal6f.TWO)))+"\" " +
                "rx=\""+r+"\" ry=\""+r+"\" "+
                "width=\""+NiceDecimal.format(width)+"\" " +
                "height=\""+NiceDecimal.format(height)+"\" " +
                "style=\"fill:black;stroke:none;\"/>\n";
    }
}
