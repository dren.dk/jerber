package dk.dren.jerber.parser.render;

import dk.dren.jerber.parser.geometry.BoundingBox;
import dk.dren.jerber.parser.geometry.Point6f;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Doesn't actually render anything, but computes a rough bounding box of all the commands rendered
 */
@Getter
@RequiredArgsConstructor
public class BoundingBoxRenderTarget implements RenderTarget {
    private BoundingBox boundingBox = BoundingBox.createUnset();

    @Override
    public void setPolarity(boolean dark) {

    }

    @Override
    public void setAperture(Aperture aperture) {

    }

    @Override
    public void moveTo(Point6f point) {
        boundingBox = boundingBox.union(point);
    }

    @Override
    public void drawTo(Point6f point) {
        boundingBox = boundingBox.union(point);
    }

    @Override
    public void flash(Point6f point) {
        boundingBox = boundingBox.union(point);
    }

    @Override
    public void arcTo(Point6f center, Point6f endPoint, boolean clockwise, boolean multiQuadrant) {
        boundingBox = boundingBox.union(endPoint).union(center);
    }

    @Override
    public void regionBegin() {

    }

    @Override
    public void regionEnd() {

    }
}
