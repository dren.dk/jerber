package dk.dren.jerber.parser.render;

import dk.dren.jerber.parser.geometry.Point6f;

public interface RenderTarget {
    /**
     * Sets the color to draw with.
     *
     * @param dark true to write dark
     */
    void setPolarity(boolean dark);

    /**
     * Sets the aperture to use when drawing
     *
     * @param aperture The aperture
     */
    void setAperture(Aperture aperture);

    /**
     * Move the current position to a point without drawing anything
     * @param point The point to move to
     */
    void moveTo(Point6f point);

    /**
     * Draw a line from the current position to a new point, using the polarity previously set with setPolarity
     * and the aperture set with setAperture
     * @param point The point to draw to
     */
    void drawTo(Point6f point);

    /**
     * Flashes the current aperture at the point
     * @param point The point at which to flash the current aperture
     */
    void flash(Point6f point);

    /**
     * Draws an arch from the current point to the end point with center in the center point.
     *
     * @param center
     * @param endPoint
     * @param clockwise
     * @param multiQuadrant
     */
    void arcTo(Point6f center, Point6f endPoint, boolean clockwise, boolean multiQuadrant);

    void regionBegin();

    void regionEnd();
}
