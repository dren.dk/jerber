package dk.dren.jerber.parser.geometry;

import lombok.Value;

/**
 * A rectangle in 2D which is defined by the minimum point and the maximum point
 */
@Value
public class BoundingBox {
    private final Point6f min;
    private final Point6f max;

    public static BoundingBox createUnset() {
        return new BoundingBox(null, null);
    }

    /**
     * Modifies this bounding box to include the other box
     *
     * @param other The other bounding box to add
     */
    public BoundingBox union(BoundingBox other) {
        if (isUnset()) {
            return other;
        }
        if (other.isUnset()) {
            return this;
        }

        return union(other.getMin()).union(other.getMax());
    }

    public BoundingBox union(Point6f point) {
        if (isUnset()) {
            return new BoundingBox(point, point);
        }

        if (isContained(point)) {
            return this;
        }

        return new BoundingBox(min.min(point), max.max(point));
    }

    public boolean isContained(Point6f point) {
        if (isUnset()) {
            return false;
        }

        return point.getX().isGreaterThanOrEqualTo(min.getX())
                && point.getX().isLessThanOrEqualTo(max.getX())
                && point.getY().isGreaterThanOrEqualTo(min.getY())
                && point.getY().isLessThanOrEqualTo(max.getY());
    }

    public boolean isUnset() {
        return min == null || max == null;
    }
}
