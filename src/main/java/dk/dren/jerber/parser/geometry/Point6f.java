package dk.dren.jerber.parser.geometry;

import lombok.NonNull;
import lombok.Value;
import org.decimal4j.immutable.Decimal6f;

/**
 * A point in 2D space with nm resolution.
 */
@Value
public class Point6f {
    @NonNull
    private final Decimal6f x;
    @NonNull
    private final Decimal6f y;

    public Point6f min(Point6f point) {
        return new Point6f(x.min(point.getX()), y.min(point.getY()));
    }

    public Point6f max(Point6f point) {
        return new Point6f(x.max(point.getX()), y.max(point.getY()));
    }

    public Decimal6f distance(Point6f other) {
        Decimal6f xd = x.subtract(other.getX());
        Decimal6f yd = y.subtract(other.getY());
        return xd.square().add(yd.square()).sqrt();
    }

    public Point6f add(Decimal6f xOffset, Decimal6f yOffset) {
        return new Point6f(x.add(xOffset), y.add(yOffset));
    }
}
